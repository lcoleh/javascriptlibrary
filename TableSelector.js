/*
*   TODO: write description
*
* */
(function(window){

    function TableSelector( options ){
        var activeClassName = options.activeClassName ? options.activeClassName : 'active',
            cursorPointer = options.cursorPointer ? options.cursorPointer : true,
            activeCellsBackground = options.activeCellsBackground ? options.activeCellsBackground : 'gray',
            defaultCellsBackground = options.defaultCellsBackground ? options.defaultCellsBackground : 'white' ,
            activeCell = null,
            nextCell = null,
            prevCell = null,
            table = jQuery( options.table ),
            items = null;



        if( table.length )
        {
            init();
            initKeyEvents();
        }

        function reInit(){
            activeCell = null;
            nextCell = null;
            prevCell = null;
            table = jQuery( options.table );
            items = null;

            init();
        }

        function init()
        {
            items = table.find('tr');

            if( cursorPointer )
            {
                items.css('cursor', 'pointer');
            }

            items.click(function()
            {
                var item = jQuery(this);

                // if active cell - make it not active
                if( item.hasClass( activeClassName ) )
                {
                    clearCell( item );
                }
                else // make cell active
                {
                    clearAllCells();
                    makeActive( item );
                }

            });
        }

        function makeActive( item )
        {
            item.addClass( activeClassName );
            item.css('background', activeCellsBackground);
            activeCell = item;
        }

        function clearCell( item )
        {
            item.removeClass( activeClassName );
            item.css('background', defaultCellsBackground);
            activeCell = null;
        }

        function clearAllCells()
        {
            items.removeClass( activeClassName );
            items.css('background', defaultCellsBackground);
        }

        function goDown()
        {
            nextCell = activeCell.next();
            if( nextCell.length ){
                clearCell( activeCell );
                makeActive( nextCell );
            }
        }

        function goUp()
        {
            prevCell = activeCell.prev();
            if( prevCell.length ){
                clearCell( activeCell );
                makeActive( prevCell );
            }
        }

        function initKeyEvents()
        {
            document.addEventListener('keydown', keyDown);
            function keyDown(e){
                if( activeCell && activeCell.length ){
                    switch( e.keyCode ){
                        case 40: // down
                            goDown();
                            break;
                        case 38: // up
                            goUp();
                            break;
                    }
                }
            }
        }

        return {
            reInit : reInit
        }
    }

    window.TableSelector = TableSelector;

})(window);

/*
 <script type="text/javascript">
 var tableSelector = new TableSelector({
 table : '.table',
 cursorPointer : true,
 activeCellsBackground : '#F5F5F5',
 defaultCellsBackground: 'white',
 activeClassName: 'active'
 });


 //tableSelector.reInit();
 </script>
 */


/*
 <table class='test' border="1">
 <tr>
 <td>row 1, cell 1</td>
 <td>row 1, cell 2</td>
 </tr>
 <tr>
 <td>row 2, cell 1</td>
 <td>row 2, cell 2</td>
 </tr>
 <tr>
 <td>row 2, cell 1</td>
 <td>row 2, cell 2</td>
 </tr>
 <tr>
 <td>row 2, cell 1</td>
 <td>row 2, cell 2</td>
 </tr>
 </table>
 */