AUI().add('pap-cost-overview', function(A) {
Liferay.namespace('Pap.CostOverview');

var j = null,
    COST_ITEM = '.pap-co-sticker',
    CATEGORY_DIALOG_HEIGHT = "auto",
    CATEGORY_DIALOG_WIDTH = 400,
    ViewEnum = {
        OVERVIEW: 'overview',
        BAR: 'bar',
        PIE: 'pie',
        ADD_EXPENSE: 'addExpense',
        ADD_FIXED_COST: 'addFixedCost'
    },
    Views = [
        {
            id:'overview',
            container:'ovContainer',
            filterContainer:'ovFilterContainer',
            showButton:'showOverview'
        },

        {
            id:'bar',
            container:'barContainer',
            filterContainer: 'barFilterContainter',
            showButton:'showBar'
        },

        {
            id:'pie',
            container: 'pieContainer',
            filterContainer: 'pieFilterContainer',
            showButton:'showPie'
        },

        {
            id:'addExpense',
            container: 'addExpenseContainer',
            showButton:'addExpense'
        },

        {
            id:'addFixedCost',
            container: 'addFixedCostContainer',
            showButton:'addFixedCost'
        }//,
        //{id:'fixedCostCycle', container:'fixedCostCyclesContainer'}
    ],
    PreviousView = ViewEnum.OVERVIEW,
    CurrentView = ViewEnum.OVERVIEW,
    ITEM_DIALOG_HEIGHT = "auto",
    ITEM_DIALOG_WIDTH = 820,
    PARTIAL_URL_ADD_SCAN = '&action=add_scan';

// this is used to get the unique value.
Math.uuidFast = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g,
        function(c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
};

function Filter() {
    // set up a uuid name for the current function.
    var _uuidName = Math.uuidFast(),
        FILTER_CHANGED = _uuidName,
        ORIGINAL_ORDER = 'original-order',
        DAY = 'day',
        AMOUNT = 'amount',
        _category = '',
        _costType = '',
        _sortAscending = '',
        _sortField = ORIGINAL_ORDER,
        _periodRange = 1;

    this.setCategory = function(value) {
        if (value != '-1') {
            _category = '.' + value;
        }
        else {
            _category = '';
        }
        this._filterChanged();
    };

    this.getCategory = function() {
        return _category.replace(/./i, '');
    };

    this.setCostType = function(value) {
        if (value != '-1') {
            _costType = '.' + value;
        }
        else {
            _costType = '';
        }
        this._filterChanged();
    };

    this.getCostType = function() {
        return _costType.replace(/./i, '');
    };


    this.getPeriodRange = function() {
        return _periodRange;
    };

    this.setPeriodRange = function(range) {
        _periodRange = range;
        this._filterChanged();
    };

    this.setSorting = function(value) {
        switch (value) {
            case 'old-to-new':
                _sortField = DAY;
                _sortAscending = true;
                break;
            case 'expensive-to-cheap':
                _sortField = AMOUNT;
                _sortAscending = false;
                break;
            case 'cheap-to-expensive':
                _sortField = AMOUNT;
                _sortAscending = true;
                break;
            default:
                // new-to-old
                _sortField = DAY;
                _sortAscending = false;
        }
        this._filterChanged();
    };

    this.getOptions = function() {

        var options = {    };

        if (_category != '' || _costType != '') {
            options['filter'] = _category + _costType;
        }
        else {
            options['filter'] = '';
        }

        options['sortBy'] = _sortField;
        if (_sortAscending !== '') {
            options[ 'sortAscending' ] = _sortAscending;
        }

        return options;
    };

    this._filterChanged = function() {
        A.fire(FILTER_CHANGED);
        Pap.fontSizeShrink();
    };

    this.onChanged = function(callback) {
        A.on(FILTER_CHANGED, callback);
        return callback;
    };
}

;


Liferay.Pap.CostOverview = {
    /**
     * Main init function.
     */
    init: function(params) {
        var instance = this;

        instance.params = params;
        instance.namespace = params.namespace;
        instance.resourceUrl = params.resourceUrl;
        instance.overviewUrl = params.overviewUrl;
        instance.addFixedCostUrl = params.addFixedCostUrl;
        instance.addExpenseUrl = params.addExpenseUrl;
        instance.barUrl = params.barUrl;
        instance.pieUrl = params.pieUrl;
        instance.itemDetailsUrl = params.itemDetailsUrl;
        instance.fixedCostCycleUrl = params.fixedCostCycleUrl;
        instance.ovContainer = A.one('#' + instance.namespace + 'ovContainer');
        instance.manageCategory = A.one("#" + instance.namespace + "manage_category");

        instance.fixedCostOverviewFlag = false; // need for reload content

        // the initialization of custom tags elements which are located in the tags WEB-INF/tags.
        j = Liferay[instance.namespace];
        
        // the filter for overview filter panel.
        instance.ovFilter = new Filter();
        // the filter for bar filter panel.
        var barFilter = new Filter(),
            // the filter for pie  filter panel.
            pieFilter = new Filter(),
            // the filter for sort items by Old to new , New to Old, etc.
            sortFilter = new Filter();

        instance.normalizedFixedCostCheckbox = A.one('#' + instance.namespace + 'normalizedFixedCostCheckbox');
        instance.normalizedFixedCost = A.one('#' + instance.namespace + 'normalizedFixedCost');

        Pap.fontSizeShrink();

        // auto load popup
        var autoLoadCostEntryId = A.one('#' + instance.namespace + 'autoLoadCostEntryId').get('innerHTML');
        if (autoLoadCostEntryId != '0') {
            showCostPopup(autoLoadCostEntryId);
        }

        // switcher class here
        instance.costOverviewButtonSwitcher = new ButtonsSwitcherClass();
        var buttons = [
            'showOverview',
            'showBar',
            'showPie',
            'addExpense',
            'addFixedCost'
        ],
            activeButtonClass = 'pap-co-button-active';
        instance.costOverviewButtonSwitcher.init(buttons, activeButtonClass, instance.namespace);

        j.showOverview.onClick(function(e) {
            instance.costOverviewButtonSwitcher.setActiveButton(e.currentTarget.get('id'));
            instance.hardCodeIsotope();
            instance.showOverview(ViewEnum.OVERVIEW);
        });

        A.on(instance.namespace + 'costItem_Deleted', function(e) {
            instance._viewLoader(ViewEnum.OVERVIEW, instance.overviewUrl);
            instance.itemDetailsDialog.close();
        });

        A.on(instance.namespace + 'costItem_Updated', function(e) {
            instance._viewLoader(ViewEnum.OVERVIEW, instance.overviewUrl);
        });

        j.showBar.onClick(function(e) {
            //instance._setVisibleView(ViewEnum.BAR);
            instance.costOverviewButtonSwitcher.setActiveButton(e.currentTarget.get('id'));
            instance._viewLoader(ViewEnum.BAR, instance.barUrl);
        });

        j.showPie.onClick(function(e) {
            //instance._setVisibleView(ViewEnum.PIE);
            instance.costOverviewButtonSwitcher.setActiveButton(e.currentTarget.get('id'));

            var url = instance.pieUrl;
            instance._viewLoader(ViewEnum.PIE, url);
        });


        j.addFixedCost.onClick(function() {
            instance.loadFixedCostOverview();
        });

        j.addExpense.onClick(function(e) {
            instance.showAddExpenseForm();
        });

        A.on(instance.namespace + 'addExpenseView_Loaded', function() {
            j.saveCost.onClick(function(e) {
                var form = A.one('#' + instance.namespace + 'costForm');
                A.io(form.get('action'), {
                    method: 'POST',
                    sync: true,
                    form:{
                        id: form.get('id')
                    },
                    on: {
                        complete: function(id, o) {
                            var callback = A.JSON.parse(o.responseText);
                            if (callback.success) {
                                A.log(callback);
                            } else {
                                var message = Liferay.Pap.HelperUtil.exceptionHandler(callback);
                                j.costOverviewInfo.showMessage(message, 'error');
                            }
                        }
                    }
                });
            });
        });


        /**
         * Cost Item SECTION.
         * */
        j.ovCostTypeFilter.onChange(function() {
            instance.ovFilter.setCostType(j.ovCostTypeFilter.getValue());
        });

        j.ovSortFilter.onChange(function() {
            sortFilter.setSorting(j.ovSortFilter.getValue());
        });


        sortFilter.onChanged(function(e) {
            var selectedIndexs = j.ovSortFilter.getElement().getDOM().selectedIndex;
            if (selectedIndexs <= 1) {

                instance._viewLoader(
                    CurrentView,
                    instance._getViewCurrentUrl(),
                    {
                        isOrderedByMonth:
                            selectedIndexs == 0 ? false : true
                    }
                );

                setTimeout(function() {
                    A.log('_viewLoaderLoaded');
                    instance.ovFilter.setSorting(j.ovSortFilter.getValue());
                }, 500);


            }
        });

        instance.ovFilter.onChanged(function(e) {
            var options = instance.ovFilter.getOptions(),
                key = null;
            A.log(options);
            for (key in Liferay[instance.namespace].costItems) {
                var totalCost = 0,
                    container = Liferay[instance.namespace].costItems[key];
                    itemsTotalSum = A.one('#total'
                    + key.replace(/container/i, ''));
                container.isotope(options);
                A.one('#' + key).all(COST_ITEM).each(function(costItem) {
                    if (!costItem.hasClass('isotope-hidden')) {
                        var amount = costItem.one('.pap-co-expense-amount').get('innerHTML');
                        //A.log( amount );
                        /*
                         * we have numbers, that are splited by '.'
                         * so we delete '.' to calculate correct numbers
                         */
                        amount = amount.replace(/\./gi, "");
                        
                        amount = amount.replace(/\,/gi, ".");

                        if (amount) {
                            totalCost += parseFloat(amount); //parseInt(amount, 10);
                        }
                    }
                });
                /*
                 * Make format of our totalCost
                 */
                itemsTotalSum.set('innerHTML', Liferay.Pap.HelperUtil.getFormatedNumber(Math.round(totalCost)));
            }
        });

        //normalizedFixedCost
        j.seePreviousMonths.onClick(function() {
            var monthsAmount = Liferay[instance.namespace].Overview.monthAmount + 1,
                url = params.overviewUrl;

            // reset old containers.
            Liferay[instance.namespace].costItems = null;
            instance.ovContainer.set('innerHTML', '');

            var data = {
                monthsAmount : monthsAmount
            };
            if (!data.normalizedFixedCost && instance.normalizedFixedCost) {
                data['normalizedFixedCost'] =
                    instance.normalizedFixedCost.get('value');
            }

            jQuery('#' + instance.ovContainer.get('id')).load(url,
                data,
                function(responseText, textStatus) {

                    if (textStatus == 'success') {
                        instance._setVisibleView(ViewEnum.OVERVIEW);
                        instance.ovFilter._filterChanged();
                    } else {
                        // TODO: error message
                        instance._showLoadingFailedMessage();
                    }
                }
            );

        });

        instance.normalizedFixedCostCheckbox.on('click', function() {

            instance._viewLoader(
                CurrentView,
                instance._getViewCurrentUrl(),
                {
                    normalizedFixedCost: instance.normalizedFixedCost.get('value')
                }
            );
        });


        /**
         * Bar panel SECTION
         * */
        j.barCostTypeFilter.onChange(function() {
            barFilter.setCostType(j.barCostTypeFilter.getValue());
        });

        barFilter.onChanged(function(e) {
            var url = instance.barUrl,
                currentMonthstamp = A.one('#' + instance.namespace + 'currentMonthstamp'),
                includeMyAverage = A.one('#' + instance.namespace + 'includeMyAverage');
            if( currentMonthstamp && includeMyAverage ){
                var data = {
                    currentMonthstamp: currentMonthstamp.get('value'),
                    costCategoryId: barFilter.getCategory(),
                    costType: barFilter.getCostType() || 'UNDEFINED',
                    includeMyAverage: includeMyAverage.get('value')
                };
                instance._viewLoader(ViewEnum.BAR, url, data);
            }else{
                // do nothing
            }
        });

        instance.barCategoryFilter = A.one('#' + instance.namespace + 'barCategoryFilter');
        //set current element first element
        var currentBar = instance.barCategoryFilter.one('.current');
        //show hide category list
        instance.barCategoryFilterOverlay = new A.OverlayContext({
            stack: true,
            trigger: currentBar,
            boundingBox: instance.barCategoryFilter.one('ul'),
            showOn: 'mouseover',
            hideOn: 'mouseout'
        }).render();
        // events
        instance.barCategoryFilterOverlay.set('showOn', 'click');
        instance.barCategoryFilterOverlay.set('hideOn', 'click');


        //set current element first element
        instance.ovCategoryFilter = A.one('#' + instance.namespace + 'ovCategoryFilter');

        var currentOv = instance.ovCategoryFilter.one('.current');

        // set default value for reset funciton
        instance.filterCurrentDefaultValue = currentOv.get('innerHTML');

        //show hide category list
        instance.ovCategoryFilterOverlay = new A.OverlayContext({
            stack: true,
            trigger: currentOv,
            boundingBox: instance.ovCategoryFilter.one('ul'),
            showOn: 'mouseover',
            hideOn: 'mouseout'
        }).render();
        // events
        instance.ovCategoryFilterOverlay.set('showOn', 'click');
        instance.ovCategoryFilterOverlay.set('hideOn', 'click');

        // just set filter to default position
        function resetFilter(){
            currentBar.set('innerHTML', instance.filterCurrentDefaultValue);
            currentOv.set('innerHTML', instance.filterCurrentDefaultValue);
            instance.ovFilter.setCategory(-1);
            barFilter.setCategory(-1);
        }

        function addFilterEvent( currentTarget ){
            A.log( currentTarget );
            var filterType = currentTarget.get('parentNode')
                .get('parentNode')
                .get('parentNode')
                .get('parentNode')
                .get('id'),
                value = currentTarget.get('className'),
                current = A.one('#' + filterType).one('.current');

            switch (filterType) {
                case instance.namespace + 'barCategoryFilter':
                    current.set('innerHTML', currentTarget.get('innerHTML'));
                    barFilter.setCategory(value);
                    break;
                case instance.namespace + 'ovCategoryFilter':
                    current.set('innerHTML', currentTarget.get('innerHTML'));
                    instance.ovFilter.setCategory(value);
                    break;
                default:
                    /* do nothing */
                    break;
            }
        }

        //Category and bar functional functional
        A.all('.' + instance.namespace + 'category_list').all('li').on('click', function(e) {
            addFilterEvent( e.currentTarget );
        });

        var manageCategory = A.all('.' + instance.namespace + 'manage_category');
        manageCategory.on('click', function() {
            //hide overlays
            instance._hideOverlays();
            instance._showCategoryManagmentDialog(instance.categoryDialogContent);
            instance._showCostForm("manage_category");
        });

        var newCategory = A.all('.' + instance.namespace + 'new_category');
        newCategory.on('click', function() {
            instance.addNewCategoryDialog();
        });


        /**
         * Pie panel SECTION
         * */
        j.pieCostTypeFilter.onChange(function() {
            pieFilter.setCostType(j.pieCostTypeFilter.getValue());
        });

        j.pieRangeFilter.onChange(function(e) {
            pieFilter.setPeriodRange(j.pieRangeFilter.getValue());
        });

        pieFilter.onChanged(function(e) {
            var url = instance.pieUrl,
                currentMonthstamp = A.one('#' + instance.namespace + 'pieCurrentMonthstamp').get('value'),
                data = {
                    currentMonthstamp: currentMonthstamp,
                    costType: pieFilter.getCostType() || 'UNDEFINED',
                    range: pieFilter.getPeriodRange()
                };
            instance._viewLoader(ViewEnum.PIE, url, data);
        });

        /**
         * Category Management SECTION
         * */

         //Category management object
        instance.categoryDialogContent = A.one('#' + instance.namespace + 'categoryDialogContent');

        var categoryIdToDelete = null;

        //manage form functional
        instance.manageCategory.all(".pap-thm-control-button-edit").on("click", function(e){
            addEditCategoryEvent( e );
        });
        instance.manageCategory.all(".pap-thm-control-button-del").on("click", function(e){
            addDeleteCategoryEvent( e );
        });
        function addDeleteCategoryEvent( note ){
            var categoryId = note.currentTarget.get('id').replace("manage-delete-category-",""),
                categoryName = A.one('#manage-category-' + categoryId).one('.name').text();
            categoryIdToDelete = categoryId;
            instance._showCostForm('delete_category', 'delete', categoryId, categoryName);
        }
        function addEditCategoryEvent( note ){
            var categoryId = note.currentTarget.get('id').replace("manage-edit-category-",""),
                categoryName = A.one('#manage-category-' + categoryId).one('.name').text();
            instance._showCostForm('edit_category', 'edit', categoryId, categoryName);
        }

        //Delete category
        A.delegate('click', function(e) {
            var buttonId = e.currentTarget.get('id');
            switch (buttonId) {
                case instance.namespace + 'deleteCategory':
                    var callback =
                        Liferay.Pap.HelperUtil.executeAjax('delete_user_category', {
                            categoryId: categoryIdToDelete
                        });
                    if ( callback.success == true ) {
                        removeCategory(categoryIdToDelete, 'ovCategoryFilter');
                        removeCategory(categoryIdToDelete, 'barCategoryFilter');
                        removeCategory(categoryIdToDelete, 'manage_category');

                        hideManageButton();

                        // set filter to default
                        resetFilter();
                    } else {
                        // TODO: excpetion .
                        var message = Liferay.Pap.HelperUtil.exceptionHandler(callback);
                        j.costOverviewInfo.showMessage(message, 'error');
                    }
                    instance.categoryDialog.close();
                    break;
                case instance.namespace + 'cancelDeleteCategory':
                    instance._showCostForm("manage_category");
                    break;
            }
        }, instance.categoryDialogContent.one('#' + instance.namespace + 'delete_category'), 'input');

        instance.newCategoryForm =
            A.one('#' + instance.namespace + 'newCategoryForm');
        instance.editCategoryForm =
            A.one('#' + instance.namespace + 'editCategoryForm');

        //New category
        j.saveNewCategory.onClick(function(e) {
            var categoryTitle = j.categoryNewName.getValue();
            if( !categoryTitle ){
                return false;
            }
            var callback = Liferay.Pap.HelperUtil.executeAjax('add_user_category', {
                    title: categoryTitle
                });
            if ( callback.success == true ) {
                //make changes in frontend
                addCategory(categoryTitle, 'ovCategoryFilter', callback.categoryId, true);
                addCategory(categoryTitle, 'barCategoryFilter', callback.categoryId, true);
                addCategory(categoryTitle, 'manage_category', callback.categoryId, true);

                // dynamicly add event to category element
                A.all(".category-id-" + callback.categoryId).on("click", function( e ){
                    addFilterEvent( e.currentTarget );
                });

                //show manage category buttons
                A.all('.' + instance.namespace + 'manage_category').show();

                // reload contant if needed
                if( instance.fixedCostOverviewFlag ){
                    instance.fixedCostOverviewFlag = false;
                    instance.loadFixedCostOverview();
                }

            } else {
                // TODO: excpetion .
                var message = Liferay.Pap.HelperUtil.exceptionHandler(callback);
                j.costOverviewInfo.showMessage(message, 'error');
            }

            instance.categoryDialog.close();
        });
        //Edit category
        j.saveEditCategory.onClick(function(e) {
            var categoryTitle = j.categoryEditName.getValue(),
                categoryId = j.categoryEditId.getValue(),
                callback = Liferay.Pap.HelperUtil.executeAjax('update_user_category', {
                    title: categoryTitle,
                    categoryId: categoryId
                });

            if (callback.success == true) {
                //make changes in frontend
                addCategory(categoryTitle, 'ovCategoryFilter', categoryId, false);
                addCategory(categoryTitle, 'barCategoryFilter', categoryId, false);
                addCategory(categoryTitle, 'manage_category', categoryId, false);
            } else {
                // TODO: excpetion .
                var message = Liferay.Pap.HelperUtil.exceptionHandler(callback);
                j.costOverviewInfo.showMessage(message, 'error');
            }

            instance.categoryDialog.close();
        });

        function addCategory(title, toAppend, categoryId, newCategory) {
            var appendTo = A.one('#' + instance.namespace + toAppend).one('.' + instance.namespace + 'category_list');

            if (newCategory == true) {
                //add category to the list
                switch (toAppend) {
                    case 'ovCategoryFilter':
                        appendTo.append('<li class="category-id-' + categoryId + '">' + title + '</li>');
                        break;
                    case 'barCategoryFilter':
                        appendTo.append('<li class="' + categoryId + '">' + title + '</li>');
                        break;
                    case 'manage_category':
                        appendTo.append('<li id="manage-category-' + categoryId + '">' +
                            '<div class="actions">' +
                                '<div id="manage-edit-category-' + categoryId + '" class="pap-thm-control-button-edit"></div>' +
                                '<div id="manage-delete-category-' + categoryId + '" class="pap-thm-control-button-del"></div>' +
                            '</div>' +
                            '<div class="name">' +
                                title +
                            '</div>' +
                            '</li>'
                        );
                        // add events to buttons
                        A.one("#manage-edit-category-" + categoryId).on("click", function( e ){
                            addEditCategoryEvent( e );
                        });
                        A.one("#manage-delete-category-" + categoryId).on("click", function( e ){
                            addDeleteCategoryEvent( e );
                        });
                        break;
                    default:
                        break;
                }
            } else {
                switch (toAppend) {
                    case 'ovCategoryFilter':
                        appendTo.all('li').each(function(node) {
                                if (node.get('className') == ( 'category-id-' + categoryId )) {
                                    node.set('innerHTML', title);
                                }
                            });
                        break;
                    case 'barCategoryFilter':
                        appendTo.all('li').each(function(node) {
                                if (node.get('className') == categoryId) {
                                    node.set('innerHTML', title);
                                }
                            });
                        break;
                    case 'manage_category':
                        appendTo.all('li').each(function(node) {
                            if (node.get('id') == ( "manage-category-" + categoryId )) {
                                    node.one('.name').set('innerHTML', title);
                                }
                            });
                        break;
                }
            }
        }

        //categoryIdToDelete, 'manage_category'
        function removeCategory(categoryId, deleteFrom) {
            A.all('.' + instance.namespace + 'category_list').all('li').each(function(node) {
                switch (deleteFrom) {
                    case 'ovCategoryFilter':
                        if (node.get('className') == ( 'category-id-' + categoryId )) {
                            node.remove(true);
                        }
                        break;
                    case 'barCategoryFilter':
                        if (node.get('className') == categoryId) {
                            node.remove(true);
                        }
                        break;
                    case 'manage_category':
                        if (node.get('id') == ( "manage-category-" + categoryId )) {
                            node.remove(true);
                        }
                        break;
                }
            });
        }

        function hideManageButton() {
            var hasChild = A.one('#' + instance.namespace + 'manage_category').
                one('.' + instance.namespace + 'category_list').
                get('childElementCount');
            if (hasChild == 0) {
                //hide manage buttons
                A.all('.' + instance.namespace + 'manage_category').hide();
            }
        }

        j.cancelNewCategory.onClick(function() {
            instance.categoryDialog.close();
        });
        j.cancelEditCategory.onClick(function() {
            instance.categoryDialog.close();
        });
        j.addCategory.onClick(function() {
            instance._showCostForm('new_category');
        });

        A.on(instance.namespace + 'costItem_Click', function(e) {
            //show popup
            var url = instance.itemDetailsUrl,
                data = {
                    itemId: e.itemId
                },
                container = A.one('#' + instance.namespace + 'itemDetailsContent');

            jQuery('#' + container.get('id')).load(url, data,
                function(responseText, textStatus) {
                    if (textStatus == 'success') {
                        var bodyContent =
                            A.one('#' + instance.namespace + 'itemDetailsContent');

                        instance._initControlsForFixedCost(e.itemId);

                        if (instance.itemDetailsDialog && instance.itemDetailsDialog.isVisibleNow == true) {
                            instance.itemDetailsDialog.bodyContent = bodyContent;
                        } else {
                            instance._showItemDetailsDialog(bodyContent);
                        }
                    } else {
                        // TODO: error message
                        instance._showLoadingFailedMessage();
                    }
                }
            );
        });

        A.on(instance.namespace + 'costItem_Updated', function(e) {
            showCostPopup(e.itemId);
        });

        // cost popup form
        function showCostPopup(itemId) {
            var url = instance.itemDetailsUrl,
                data = {
                    itemId: itemId
                },
                container = A.one('#' + instance.namespace + 'itemDetailsContent');

            jQuery('#' + container.get('id')).load(url, data,
                function(responseText, textStatus) {
                    if (textStatus == 'success') {
                        var bodyContent = A.one('#' + instance.namespace + 'itemDetailsContent');

                        instance._initControlsForFixedCost(itemId);

                        if (instance.itemDetailsDialog && instance.itemDetailsDialog.isVisibleNow == true) {
                            instance.itemDetailsDialog.bodyContent = bodyContent;
                        } else {
                            instance._showItemDetailsDialog(bodyContent);
                        }
                    } else {
                        // TODO: error message
                        instance._showLoadingFailedMessage();
                    }
                }
            );
        }

        /*
         * top buttons functional
         * */
        A.one('#' + instance.namespace + 'addExpense').on('click', function(e) {
            instance.costOverviewButtonSwitcher.setActiveButton(e.currentTarget.get('id'));
        });
        A.one('#' + instance.namespace + 'addFixedCost').on('click', function(e) {
            instance.costOverviewButtonSwitcher.setActiveButton(e.currentTarget.get('id'));
        });
    },

    _showCostForm : function( formId, action, categoryId, categoryName ){
        var instance = this,
            form = A.one('#' + instance.namespace + formId);

        instance._hideCostForms();

        switch (action) {
            case 'edit':
                form.one('#' + instance.namespace + 'categoryEditName').set('value', categoryName);
                form.one('#' + instance.namespace + 'categoryEditId').set('value', categoryId);
                form.show();
                break;
            case 'delete':
                form.one('#' + instance.namespace + 'categoryDeleteName').set('innerHTML', categoryName);
                form.one('#' + instance.namespace + 'categoryDeleteId').set('value', categoryId);
                form.show();
                break;
            default:
                form.show();
                break;
        }
    },

    _initControlsForFixedCost: function(itemId) {
        var instance = this,
            addFixedSliderScan = A.one('#' + instance.namespace + 'addFixedSliderScan'),
            addFixedCostScan = A.one('#' + instance.namespace + 'addFixedCostScan'),
            editFixedCost = A.one('#' + instance.namespace + 'editFixedCost');

        instance.fixedCostItemId = itemId;

        addFixedSliderScan.on('click', function(e) {
            //make button active
            instance.costOverviewButtonSwitcher.setActiveButton(instance.namespace + 'addFixedCost');
            instance._goToUploadFixedCostScans();
        });

        addFixedCostScan.on('click', function(e) {
            //make button active
            instance.costOverviewButtonSwitcher.setActiveButton(instance.namespace + 'addFixedCost');
            instance._goToUploadFixedCostScans();
        });

        editFixedCost.on('click', function(e) {
            instance._viewLoader(
                ViewEnum.ADD_FIXED_COST,
                instance._getAddFixedCostUrl(instance.fixedCostItemId,
                    'fixedCostItem', instance.fixedCostCycleUrl, 'costEntryId'));
            instance.itemDetailsDialog.close();
        });
    },

    _goToUploadFixedCostScans: function() {
        var instance = this,
            url = instance.fixedCostCycleUrl + PARTIAL_URL_ADD_SCAN;
        instance._viewLoader(
            ViewEnum.ADD_FIXED_COST,
            instance._getAddFixedCostUrl(instance.fixedCostItemId,
                'fixedCostItem', url, 'costEntryId'));
        instance.itemDetailsDialog.close();
    },

    //simple helper function to hide and show forms
    _hideCostForms: function() {
        var instance = this;
        instance.categoryDialogContent.one('#' + instance.namespace + 'new_category').hide();
        instance.categoryDialogContent.one('#' + instance.namespace + 'edit_category').hide();
        instance.categoryDialogContent.one('#' + instance.namespace + 'manage_category').hide();
        instance.categoryDialogContent.one('#' + instance.namespace + 'delete_category').hide();
    },
    /**
     * Shows the item details dialog.
     * */
    _showItemDetailsDialog: function(bodyContent) {
        var instance = this;

        if (!instance.itemDetailsDialog) {
            instance.itemDetailsDialog = new A.Dialog({
                bodyContent: bodyContent,
                draggable: false,
                modal: true,
                resizable: false,
                centered: true,
                close: true,
                cssClass: "pap-modal-dialog",
                stack: true,
                height: "auto",
                width: ITEM_DIALOG_WIDTH
            }).render();
            instance.itemDetailsDialog.on('close', function() {
                //instance.Form.reset();
                instance.itemDetailsDialog.isVisibleNow = false;
                Liferay.Pap.CostLightbox.destroySlider();
            });
        }
        else {
            instance.itemDetailsDialog.show();
            instance.itemDetailsDialog.isVisibleNow = true;
        }
    },
    /**
     * This functions shows category management dialog window.
     * .....
     * */
    _showCategoryManagmentDialog: function(bodyContent) {
        var instance = this;

        if (!instance.categoryDialog) {
            instance.categoryDialog = new A.Dialog({
                zIndex: 1000,
                bodyContent: bodyContent,
                draggable: false,
                modal: true,
                resizable: false,
                centered: true,
                close: true,
                cssClass: "pap-modal-dialog",
                stack: true,
                height: CATEGORY_DIALOG_HEIGHT,
                width: CATEGORY_DIALOG_WIDTH
            }).render();
            instance.categoryDialog.on('close', function() {
                //instance.tabs.selectTab(TabEnum.REGISTRATION);
                // reset all info panels, each panel has its own listener for this event.
                //A.fire('HideAllInfoPanels');
                // reset all forms after close.
                instance.newCategoryForm.reset();
                instance.editCategoryForm.reset();
                instance._hideCostForms();
                //instance.registrationForm.reset();
            });
        }
        else {
            instance.categoryDialog.show();
        }
    },

    /**
     * Gets the current url of the current view.
     * */
    _getViewCurrentUrl: function() {
        var instance = this;
        return instance[CurrentView + 'Url'];
    },


    _getAddFixedCostUrl: function(target, pattern, url, idName) {
        var instance = this,
            id = target,
            result = url.replace(idName + '=0', idName + '=' + id);
        return result;
    },

    /**
     * Loads appropriate view with a specified URL.
     * */
    _viewLoader: function(viewName, url, data) {
        var instance = this,
            targetContainer = '',
            monthsAmount = Liferay[instance.namespace].Overview.monthAmount;

        if (data && !data.monthsAmount && monthsAmount > 0) {
            data['monthsAmount'] = monthsAmount;
        }
        if (data && !data.normalizedFixedCost && instance.normalizedFixedCost) {
            data['normalizedFixedCost'] =
                instance.normalizedFixedCost.get('value');
        }

        for (var key in Views) {
            if (Views[key].id == viewName) {
                targetContainer = Views[key].container;
            }
        }

        jQuery('#' + instance.namespace + targetContainer).load(url, data,
            function(responseText, textStatus) {
                if (textStatus == 'success') {
                    instance._setVisibleView(viewName);
                } else {
                    // TODO: error message
                    instance._showLoadingFailedMessage();
                }
            }
        );
    },

    /**
     * Sets visibility of the view which should be displayed.
     * @param viewName the view name.
     * */
    _setVisibleView: function(viewName) {
        var instance = this,
            indexViewToShow = 0,
            viewContainer = '',
            filterContainer = '',
            showButton = '';

        PreviousView = CurrentView;
        CurrentView = viewName;

        for (var key in Views) {
            // assign a view's index which should be shown.
            if (Views[key].id == viewName) {
                indexViewToShow = key;
            }

            viewContainer = A.one('#' + instance.namespace + Views[key].container);
            filterContainer = A.one('#' + instance.namespace + Views[key].filterContainer);
            showButton = A.one('#' + instance.namespace + Views[key].showButton);

            // hide all views, filters, deactivate buttons.
            if (viewContainer) {
                viewContainer.hide();
            }
            if (filterContainer) {
                filterContainer.hide();
            }
        }

        viewContainer = A.one('#' + instance.namespace + Views[indexViewToShow].container);
        filterContainer = A.one('#' + instance.namespace + Views[indexViewToShow].filterContainer);
        showButton = A.one('#' + instance.namespace + Views[indexViewToShow].showButton);
        // show selected the view, the filter, the button.
        var select = A.one('#' + instance.namespace + Views[indexViewToShow].container + ' select');
        if (select && !select.hasClass('aui-helper-hidden')) {
            Liferay.Pap.SelectWrapper.createMarkUp(select, 100);
        }


        if (filterContainer) {
            filterContainer.show();
        }

        instance._viewContainerChanged();
        viewContainer.show();
    },

    /**
     * Used for the additional show/hide implementation of a dedicated view.
     * */
    _viewContainerChanged: function() {
        var instance = this,
            normalizedFixedCost = A.one('#' + instance.namespace + 'normalizedFixedCostPanel').hide(),
            seePreviousMonths = A.one('#' + instance.namespace + 'seePreviousMonths').hide();

        switch (CurrentView) {
            case ViewEnum.OVERVIEW:
                normalizedFixedCost.show();
                seePreviousMonths.show();
                break;
            case ViewEnum.BAR:
                normalizedFixedCost.show();
                break;
            case ViewEnum.PIE:
                normalizedFixedCost.show();
                break;
            default:
                break;
        }
    },

    /**
     This function is hiding all needed overlays
     */
    _hideOverlays: function() {
        var instance = this;

        if (instance.ovCategoryFilterOverlay) {
            instance.ovCategoryFilterOverlay.hide();
        }
        if (instance.barCategoryFilterOverlay) {
            instance.barCategoryFilterOverlay.hide();
        }

        if (Liferay.Pap.CarSelector) {
            Liferay.Pap.CarSelector.hideCarSelectorOverlay();
        }
    },

    /**
     *     This function is show error message
     */
    _showLoadingFailedMessage: function() {
        callback.exception = 'LoadingIsFailedException';
        var message = Liferay.Pap.HelperUtil.exceptionHandler(callback);
        j.costOverviewInfo.showMessage(message, 'error');
    },

    /**
     * This function is sowing overview and set,
     * if 'setVisibleView' view
     *
     * */
    showOverview: function(setVisibleView) {
        var instance = this,
            url = instance.params.overviewUrl;
        // reset old containers.
        Liferay[instance.namespace].costItems = null;
        instance.ovContainer.set('innerHTML', '');

        jQuery('#' + instance.ovContainer.get('id')).load(url,
            function(responseText, textStatus) {

                if (textStatus == 'success') {
                    if (setVisibleView) {
                        instance._setVisibleView(setVisibleView);
                    }
                    instance.ovFilter._filterChanged();
                    Pap.fontSizeShrink();
                } else {
                    // TODO: error message
                    instance._showLoadingFailedMessage();
                }
            }
        );
    },

    /**
     * This function is show AddExpense form
     * */
    showAddExpenseForm: function() {
        var instance = this,
            url = instance.params.addExpenseUrl;

        jQuery('#' + instance.namespace + 'addExpenseContainer').load(url,
            function(responseText, textStatus) {

                if (textStatus == 'success') {
                    instance._setVisibleView(ViewEnum.ADD_EXPENSE);
                } else {
                    // TODO: error message
                    instance._showLoadingFailedMessage();
                }
            }
        );
    },

    /**
     * This function returns previous view
     * */
    getPreviousView: function() {
        return PreviousView;
    },

    /**
     * This function returns current view
     * */
    getCurrentView: function() {
        return CurrentView;
    },

    /**
     *  Hardcode: it helps to make correct margin top for isotope
     * */
    hardCodeIsotope: function() {
        var instance = this;
        instance.ovContainer.show();
    },

    /**
     *  Show add new category dialog
     */
    addNewCategoryDialog: function( reloadFixedCostContent ){
        var instance = this;
        //hide overlays
        instance._hideOverlays();
        instance._showCategoryManagmentDialog( instance.categoryDialogContent );
        instance._showCostForm("new_category");
        instance.fixedCostOverviewFlag = reloadFixedCostContent;
    },

    /**
    *   Upload / reload fixed cost overview content
    * */
    loadFixedCostOverview: function(){
        var instance = this;
        instance._viewLoader( ViewEnum.ADD_FIXED_COST, instance.addFixedCostUrl );
    }

};
},
'',
{
requires: [
    'aui-base',
    'aui-io-request',
    'json',
    'event',
    'event-delegate',
    'pap-helper-util',
    'pap-login-registration',
    'pap-car-selector',
    'pap-selector-wrapper']
});
